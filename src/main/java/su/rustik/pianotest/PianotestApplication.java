package su.rustik.pianotest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PianotestApplication {

    public static void main(String[] args) {
        SpringApplication.run(PianotestApplication.class, args);
    }

}
