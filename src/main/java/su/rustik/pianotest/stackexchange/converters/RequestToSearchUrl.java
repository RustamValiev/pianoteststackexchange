package su.rustik.pianotest.stackexchange.converters;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import su.rustik.pianotest.stackexchange.entity.request.search.Request;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Конвертер entity запроса сервиса search в URI для REST запроса
 */
@Component
public class RequestToSearchUrl implements Converter<Request, URI> {

    @Override
    public URI convert(Request source) {
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http")
                .setHost("api.stackexchange.com")
                .setPath("/2.2/search");

        builder.addParameters(toList(source));
        try{
            return builder.build();
        } catch (URISyntaxException e) {
            return null;
        }
    }

    private List<NameValuePair> toList(Request source){
        List<NameValuePair> params = new LinkedList<>();
        params.add(toLinkParameter("order", source.getOrder()));
        params.add(toLinkParameter("intitle", source.getIntitle()));
        params.add(toLinkParameter("page", source.getPage()));
        params.add(toLinkParameter("fromdate", source.getFromdate()));
        params.add(toLinkParameter("max", source.getMax()));
        params.add(toLinkParameter("min", source.getMin()));
        params.add(toLinkParameter("nottaged", source.getNottagged()));
        params.add(toLinkParameter("pagesize", source.getPagesize()));
        params.add(toLinkParameter("sort", source.getSort()));
        params.add(toLinkParameter("tagged", source.getTagged()));
        params.add(toLinkParameter("site", source.getSite()));
        params.add(toLinkParameter("filter", source.getFilter()));
        params.add(toLinkParameter("todate", source.getTodate()));
        while (params.contains(null)){
            params.remove(null);
        }
        return params;
    }

    private NameValuePair toLinkParameter(String name, Enum enm){
        return enm != null ? new BasicNameValuePair(name, enm.name()) : null;
    }

    private NameValuePair toLinkParameter(String name, String value){
        return value != null && !value.isEmpty() ? new BasicNameValuePair(name, value) : null;
    }

    private NameValuePair toLinkParameter(String name, Date value){
        return value != null ? new BasicNameValuePair(name, String.valueOf(value.getTime()/1000)) : null;
    }

    private NameValuePair toLinkParameter(String name, Integer value){
        return value != null ? new BasicNameValuePair(name, String.valueOf(value)) : null;
    }
}
