package su.rustik.pianotest.stackexchange.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import su.rustik.pianotest.stackexchange.entity.response.Item;
import su.rustik.pianotest.stackexchange.entity.response.StackExchangeResponse;
import su.rustik.pianotest.stackexchange.entity.services.results.SearchResult;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Конвертер ответа от сервиса search в лист
 * для загрузки в таблицу с результатом запроса
 */
@Component
public class StackExchangeResponseToSearchResult implements Converter<StackExchangeResponse, List<SearchResult>> {
    @Override
    public List<SearchResult> convert(StackExchangeResponse source) {
        return source.getItems().stream().map(this::itemConverter)
                .collect(Collectors.toList());
    }

    private SearchResult itemConverter(Item item){
        return new SearchResult(item.getOwner().getDisplayName(),
                new Date(item.getCreationDate()*1000),
                item.getIsAnswered(),
                item.getTitle(),
                item.getLink());
    }
}
