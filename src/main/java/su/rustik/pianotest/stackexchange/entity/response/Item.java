package su.rustik.pianotest.stackexchange.entity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Item {

    private Owner owner;
    @JsonProperty("is_answered")
    private Boolean isAnswered;
    @JsonProperty("creation_date")
    private Long creationDate;
    @JsonProperty("question_id")
    private String questionId;
    private String link;
    private String title;

}
