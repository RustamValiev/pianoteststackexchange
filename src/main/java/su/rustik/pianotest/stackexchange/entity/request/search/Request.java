package su.rustik.pianotest.stackexchange.entity.request.search;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Entity запроса к сервису search
 */
@Getter
@Setter
@NoArgsConstructor
public class Request implements Serializable {

    private Integer page;
    private Integer pagesize;
    private Date fromdate;
    private Date todate;
    private Order order;
    private Date min;
    private Date max;
    private Sort sort;
    private String tagged;
    private String nottagged;
    private String intitle;
    private String filter;
    private String site;
}
