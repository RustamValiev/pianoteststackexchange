package su.rustik.pianotest.stackexchange.entity.request.search;

/**
 * Перечисление способов сортировки сервиса search
 */
public enum Sort {
    activity,
    votes,
    creation,
    relevance;
}
