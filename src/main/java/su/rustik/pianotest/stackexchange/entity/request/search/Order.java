package su.rustik.pianotest.stackexchange.entity.request.search;

/**
 * Перечисление порядка сортировки
 */
public enum Order {
    asc,
    desc;
}
