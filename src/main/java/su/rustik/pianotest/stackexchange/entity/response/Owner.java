package su.rustik.pianotest.stackexchange.entity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Owner {
    @JsonProperty("display_name")
    private String displayName;
}
