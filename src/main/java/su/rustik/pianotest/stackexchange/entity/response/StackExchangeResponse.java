package su.rustik.pianotest.stackexchange.entity.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Entity ответа от сервиса search stackexchange
 */
@Getter
@Setter
public class StackExchangeResponse implements Serializable {

    private List<Item> items;
}
