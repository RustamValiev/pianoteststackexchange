package su.rustik.pianotest.stackexchange.entity.services.results;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Подготовленный entity строки таблицы с результатом
 */
@Getter
@Setter
@AllArgsConstructor
public class SearchResult {

    private String author;
    private Date creationDate;
    private Boolean answered;
    private String title;
    private String link;


}
