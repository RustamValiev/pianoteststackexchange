package su.rustik.pianotest.stackexchange.client;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * Абстрактный класс реализующий интерфейс RestClient
 * и инициалзирующий RestTemplate
 * @param <R>
 */
public class RestClientImpl<R> implements RestClient<R> {

    private HttpClient httpClient = HttpClientBuilder.create().build();
    private ClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
    protected RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);

    @Override
    public R get(URI url) {
        return null;
    }
}
