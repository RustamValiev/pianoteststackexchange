package su.rustik.pianotest.stackexchange.client;

import java.net.URI;

@FunctionalInterface
public interface RestClient<R> {

    R get(URI url);

}
