package su.rustik.pianotest.stackexchange.client;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import su.rustik.pianotest.stackexchange.entity.response.StackExchangeResponse;

import java.net.URI;

@Component
public class Search extends RestClientImpl<StackExchangeResponse> {

    /**
     * REST-запрос GET
     * @param url - URI для запроса
     * @return
     */
    @Override
    public StackExchangeResponse get(URI url) {
        try {
            return restTemplate.getForObject(url, StackExchangeResponse.class);
        } catch (RestClientException e){
            return null;
        }
    }
}
