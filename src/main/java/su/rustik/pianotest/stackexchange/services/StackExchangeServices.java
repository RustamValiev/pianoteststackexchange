package su.rustik.pianotest.stackexchange.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import su.rustik.pianotest.stackexchange.client.RestClient;
import su.rustik.pianotest.stackexchange.converters.RequestToSearchUrl;
import su.rustik.pianotest.stackexchange.converters.StackExchangeResponseToSearchResult;
import su.rustik.pianotest.stackexchange.entity.request.search.Request;
import su.rustik.pianotest.stackexchange.entity.response.StackExchangeResponse;
import su.rustik.pianotest.stackexchange.entity.services.results.SearchResult;

import java.net.URI;
import java.util.List;

@Service
public class StackExchangeServices {

    private RestClient<StackExchangeResponse> searchClient;
    private StackExchangeResponseToSearchResult searchResultConverter;
    private RequestToSearchUrl requestToSearchUrl;

    @Autowired
    public StackExchangeServices(RestClient<StackExchangeResponse> searchClient, StackExchangeResponseToSearchResult searchResultConverter, RequestToSearchUrl requestToSearchUrl) {
        this.searchClient = searchClient;
        this.searchResultConverter = searchResultConverter;
        this.requestToSearchUrl = requestToSearchUrl;
    }

    /**
     * Сервис вызова сервиса search api stackexchange
     * @param request - Данные запроса для передачи в stackexchange
     * @return
     */
    public List<SearchResult> search(Request request){
        URI uri = requestToSearchUrl.convert(request);
        StackExchangeResponse response = searchClient.get(uri);
        if (response != null){
            return searchResultConverter.convert(response);
        }
        return null;
    }

}
