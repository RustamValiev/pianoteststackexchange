package su.rustik.pianotest.ui.pages;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import su.rustik.pianotest.ui.components.ResultTable;
import su.rustik.pianotest.ui.components.SearchComponent;

import javax.annotation.PostConstruct;

@PageTitle("StackExchange Searcher")
@Route("home")
public class HomePage extends VerticalLayout {

    @Autowired
    private SearchComponent searchComponent;
    @Autowired
    private ResultTable resultTable;

    /**
     * Добавление компонентов на главную страницу
     */
    @PostConstruct
    private void init(){
        searchComponent.setResultGrid(resultTable);
        this.add(searchComponent, resultTable);
    }

}
