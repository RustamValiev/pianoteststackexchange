package su.rustik.pianotest.ui.components;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.spring.annotation.SpringComponent;
import su.rustik.pianotest.stackexchange.entity.services.results.SearchResult;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;

@SpringComponent
public class ResultTable extends Grid<SearchResult> {

    /**
     * Добавление столбцов таблицы с правилами заполнения
     */
    @PostConstruct
    private void init(){
        this.addComponentColumn(searchResult -> {
            return new Icon(searchResult.getAnswered()?VaadinIcon.COMMENT:VaadinIcon.COMMENT_O);
        }).setHeader("Ответы")
                .setFlexGrow(0)
                .setResizable(false);

        this.addColumn(searchResult -> {
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            return formatter.format(searchResult.getCreationDate());
        }).setHeader("Дата вопроса")
                .setFlexGrow(10)
                .setResizable(true);

        this.addColumn(SearchResult::getTitle)
                .setHeader("Тема вопроса")
                .setFlexGrow(80)
                .setResizable(true);

        this.addColumn(SearchResult::getAuthor)
                .setHeader("Автор")
                .setFlexGrow(10)
                .setResizable(true);

        this.addComponentColumn(searchResult -> {
            Anchor anchor = new Anchor();
            anchor.setTarget("_blank");
            anchor.setHref(searchResult.getLink());
            anchor.add(new Icon(VaadinIcon.LINK));
            return anchor;
        }).setHeader("Ссылка")
                .setFlexGrow(0)
                .setResizable(false);
    }



}

