package su.rustik.pianotest.ui.components;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.annotation.Autowired;
import su.rustik.pianotest.stackexchange.entity.request.search.Order;
import su.rustik.pianotest.stackexchange.entity.request.search.Request;
import su.rustik.pianotest.stackexchange.entity.request.search.Sort;
import su.rustik.pianotest.stackexchange.entity.services.results.SearchResult;
import su.rustik.pianotest.stackexchange.services.StackExchangeServices;

import javax.annotation.PostConstruct;
import java.util.List;

@SpringComponent
public class SearchComponent extends VerticalLayout {

    @Autowired
    private StackExchangeServices stackExchangeServices;

    private TextField tfInTitle;
    private Button btnFind;
    private Grid<SearchResult> resultGrid;


    public void setResultGrid(Grid grid){
        this.resultGrid = grid;
    }

    /**
     * Добавление компонентов компонента поиска
     */
    @PostConstruct
    private void init(){
        this.setAlignItems(Alignment.CENTER);
        tfInTitle = new TextField("Поисковый запрос");
        tfInTitle.setWidth("100%");
        btnFind = new Button("Найти");
        btnFind.addClickListener(new ComponentEventListener<ClickEvent<Button>>() {
            @Override
            public void onComponentEvent(ClickEvent<Button> event) {
                Request request = new Request();
                request.setIntitle(tfInTitle.getValue());
                request.setFilter("!*7Pa29)flHZ7x8whzDtrPQn9Tcg)");
                request.setOrder(Order.desc);
                request.setSort(Sort.creation);
                request.setSite("stackoverflow");
                List<SearchResult> result = stackExchangeServices.search(request);
                if (result != null) {
                    resultGrid.setItems(result);
                } else {
                    Notification notify = new Notification();
                    notify.setPosition(Notification.Position.BOTTOM_END);
                    notify.setText("Ничего не найдено");
                    notify.setDuration(10);
                    notify.add(new Label(String.format("По запросу \"%s\" ничего не найдено", request.getIntitle())));
                    notify.open();
                }
            }
        });
        this.add(tfInTitle,btnFind);
    }

}
